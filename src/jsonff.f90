module jsonff
    use erloff, only: &
            ErrorList_t, &
            MessageType_t, &
            Fatal, &
            Module_, &
            Procedure_, &
            NOT_FOUND_TYPE, &
            OUT_OF_BOUNDS_TYPE
    use iso_varying_string, only: &
            VARYING_STRING, &
            assignment(=), &
            operator(//), &
            operator(==), &
            char, &
            var_str
    use parff, only: &
            ParsedCharacter_t, &
            ParsedItems_t, &
            ParsedString_t, &
            ParsedValue_t, &
            ParseResult_t, &
            ParserOutput_t, &
            State_t, &
            dropThen, &
            either, &
            EmptyOk, &
            newState, &
            many, &
            many1, &
            manyWithSeparator, &
            Message, &
            parseChar, &
            parseDigit, &
            parseString, &
            parseWhitespace, &
            parseWith, &
            repeat_, &
            satisfy, &
            sequence, &
            thenDrop, &
            withLabel
    use strff, only: &
            operator(.includes.), &
            hangingIndent, &
            join, &
            readFile, &
            toString, &
            NEWLINE

    implicit none
    private

    type, public, abstract :: JsonValue_t
    contains
        private
        procedure(valueToString), public, deferred :: toCompactString
        procedure(valueToString), public, deferred :: toExpandedString
    end type JsonValue_t

    type, public, extends(JsonValue_t) :: JsonNull_t
    contains
        private
        procedure, public :: toCompactString => nullToString
        procedure, public :: toExpandedString => nullToString
    end type JsonNull_t

    type, public, extends(JsonValue_t) :: JsonTrue_t
    contains
        private
        procedure, public :: toCompactString => trueToString
        procedure, public :: toExpandedString => trueToString
    end type JsonTrue_t

    type, public, extends(JsonValue_t) :: JsonFalse_t
    contains
        private
        procedure, public :: toCompactString => falseToString
        procedure, public :: toExpandedString => falseToString
    end type JsonFalse_t

    type, public, extends(JsonValue_t) :: JsonString_t
        private
        type(VARYING_STRING) :: string
    contains
        private
        procedure, public :: getValue => getValueFromString
        procedure, public :: toCompactString => stringToString
        procedure, public :: toExpandedString => stringToString
    end type JsonString_t

    type, public, extends(JsonValue_t) :: JsonNumber_t
        private
        double precision :: number
    contains
        private
        procedure, public :: getValue => getValueFromNumber
        procedure, public :: toCompactString => numberToString
        procedure, public :: toExpandedString => numberToString
    end type JsonNumber_t

    type, public :: JsonElement_t
        class(JsonValue_t), allocatable :: element
    end type JsonElement_t

    type, public, extends(JsonValue_t) :: JsonArray_t
        private
        integer :: num_elements = 0
        type(JsonElement_t), allocatable :: elements(:)
    contains
        private
        procedure :: appendE
        procedure :: appendV
        generic, public :: append => appendE, appendV
        procedure, public :: getElement => getElementFromArray
        procedure :: insertE
        procedure :: insertV
        generic, public :: insert => insertE, insertV
        procedure, public :: length
        procedure :: prependE
        procedure :: prependV
        generic, public :: prepend => prependE, prependV
        procedure, public :: toCompactString => arrayToCompactString
        procedure, public :: toExpandedString => arrayToExpandedString
    end type JsonArray_t

    type, public :: JsonMember_t
        type(VARYING_STRING) :: string
        class(JsonValue_t), allocatable :: element
    end type JsonMember_t

    type, public, extends(JsonValue_t) :: JsonObject_t
        private
        integer :: num_members = 0
        type(JsonMember_t), allocatable :: members(:)
    contains
        private
        procedure :: addJE
        procedure :: addJV
        procedure :: addM
        generic, public :: add => addJE, addJV, addM
        procedure :: addUnsafeCE
        procedure :: addUnsafeCV
        procedure :: addUnsafeSE
        procedure :: addUnsafeSV
        generic, public :: addUnsafe => &
                addUnsafeCE, addUnsafeCV, addUnsafeSE, addUnsafeSV
        procedure :: getElementFromObjectC
        procedure :: getElementFromObjectJ
        procedure :: getElementFromObjectS
        generic, public :: getElement => &
                getElementFromObjectC, &
                getElementFromObjectJ, &
                getElementFromObjectS
        procedure, public :: toCompactString => objectToCompactString
        procedure, public :: toExpandedString => objectToExpandedString
    end type JsonObject_t

    type, extends(ParsedValue_t) :: ParsedElement_t
        class(JsonValue_t), allocatable :: value_
    end type ParsedElement_t

    type, extends(ParsedValue_t) :: ParsedMember_t
        type(VARYING_STRING) :: string
        class(JsonValue_t), allocatable :: element
    end type ParsedMember_t

    abstract interface
        pure function valueToString(self) result(string)
            import JsonValue_t, VARYING_STRING
            class(JsonValue_t), intent(in) :: self
            type(VARYING_STRING) :: string
        end function valueToString
    end interface

    interface JsonMember
        module procedure JsonMemberJE
        module procedure JsonMemberJV
    end interface JsonMember

    interface JsonMemberUnsafe
        module procedure JsonMemberUnsafeCE
        module procedure JsonMemberUnsafeCV
        module procedure JsonMemberUnsafeSE
        module procedure JsonMemberUnsafeSV
    end interface JsonMemberUnsafe

    interface JsonObject
        module procedure JsonObjectJ
        module procedure JsonObjectM
    end interface JsonObject

    interface JsonString
        module procedure JsonStringC
        module procedure JsonStringS
    end interface JsonString

    interface JsonStringUnsafe
        module procedure JsonStringUnsafeC
        module procedure JsonStringUnsafeS
    end interface JsonStringUnsafe

    interface parseJson
        module procedure parseJsonC
        module procedure parseJsonS
    end interface parseJson

    interface parseJsonFromFile
        module procedure parseJsonFromFileC
        module procedure parseJsonFromFileS
    end interface parseJsonFromFile

    type(JsonNull_t), parameter, public :: JSON_NULL = JsonNull_t()
    type(JsonTrue_t), parameter, public :: JSON_TRUE = JsonTrue_t()
    type(JsonFalse_t), parameter, public :: JSON_FALSE = JsonFalse_t()

    type(MessageType_t), parameter, public :: INVALID_INPUT_TYPE = MessageType_t( &
            "Invalid Input")

    integer, parameter :: INDENTATION = 4
    character(len=*), parameter :: MODULE_NAME = "jsonff"

    public :: &
            JsonArray, &
            JsonElement, &
            JsonMember, &
            JsonMemberUnsafe, &
            JsonNumber, &
            JsonObject, &
            JsonObjectUnsafe, &
            JsonString, &
            JsonStringUnsafe, &
            parseJson, &
            parseJsonFromFile
contains
    pure subroutine addUnsafeCE(self, string, element)
        class(JsonObject_t), intent(inout) :: self
        character(len=*), intent(in) :: string
        type(JsonElement_t), intent(in) :: element

        call self%add(JsonMemberUnsafe(string, element))
    end subroutine addUnsafeCE

    pure subroutine addUnsafeCV(self, string, value)
        class(JsonObject_t), intent(inout) :: self
        character(len=*), intent(in) :: string
        class(JsonValue_t), intent(in) :: value

        call self%add(JsonMemberUnsafe(string, value))
    end subroutine addUnsafeCV

    pure subroutine addJE(self, string, element)
        class(JsonObject_t), intent(inout) :: self
        type(JsonString_t), intent(in) :: string
        type(JsonElement_t), intent(in) :: element

        call self%add(JsonMember(string, element))
    end subroutine addJE

    pure subroutine addJV(self, string, value)
        class(JsonObject_t), intent(inout) :: self
        type(JsonString_t), intent(in) :: string
        class(JsonValue_t), intent(in) :: value

        call self%add(JsonMember(string, value))
    end subroutine addJV

    pure subroutine addM(self, member)
        class(JsonObject_t), intent(inout) :: self
        type(JsonMember_t), intent(in) :: member

        integer :: i
        type(JsonMember_t) :: members(self%num_members)

        if (self%num_members > 0) then
            do i = 1, self%num_members
                if (member%string == self%members(i)%string) then
                    allocate(self%members(i)%element, source = member%element)
                    return
                end if
            end do
            members(:) = self%members(:)
            deallocate(self%members)
            self%num_members = self%num_members + 1
            allocate(self%members(self%num_members))
            self%members(1:self%num_members-1) = members(:)
            self%members(self%num_members) = member
        else
            self%num_members = 1
            allocate(self%members(1))
            self%members(1) = member
        end if
    end subroutine addM

    pure subroutine addUnsafeSE(self, string, element)
        class(JsonObject_t), intent(inout) :: self
        type(VARYING_STRING), intent(in) :: string
        type(JsonElement_t), intent(in) :: element

        call self%add(JsonMemberUnsafe(string, element))
    end subroutine addUnsafeSE

    pure subroutine addUnsafeSV(self, string, value)
        class(JsonObject_t), intent(inout) :: self
        type(VARYING_STRING), intent(in) :: string
        class(JsonValue_t), intent(in) :: value

        call self%add(JsonMemberUnsafe(string, value))
    end subroutine addUnsafeSV

    pure subroutine appendE(self, element)
        class(JsonArray_t), intent(inout) :: self
        type(JsonElement_t), intent(in) :: element

        type(JsonElement_t) :: temp(self%num_elements)

        if (self%num_elements > 0) then
            temp(1:self%num_elements) = self%elements(1:self%num_elements)
            deallocate(self%elements)
            allocate(self%elements(self%num_elements + 1))
            self%elements(1:self%num_elements) = temp(1:self%num_elements)
            self%num_elements = self%num_elements + 1
            self%elements(self%num_elements) = element
        else
            self%num_elements = 1
            allocate(self%elements(1))
            self%elements(1) = element
        end if
    end subroutine appendE

    pure subroutine appendV(self, value)
        class(JsonArray_t), intent(inout) :: self
        class(JsonValue_t), intent(in) :: value

        call self%append(JsonElement(value))
    end subroutine appendV

    pure function arrayToCompactString(self) result(string)
        class(JsonArray_t), intent(in) :: self
        type(VARYING_STRING) :: string

        integer :: i
        type(VARYING_STRING) :: individual_strings(self%num_elements)

        do i = 1, self%num_elements
            individual_strings(i) = self%elements(i)%element%toCompactString()
        end do
        string = "[" // join(individual_strings, ",") // "]"
    end function arrayToCompactString

    pure function arrayToExpandedString(self) result(string)
        class(JsonArray_t), intent(in) :: self
        type(VARYING_STRING) :: string

        integer :: i
        type(VARYING_STRING) :: individual_strings(self%num_elements)

        do i = 1, self%num_elements
            individual_strings(i) = self%elements(i)%element%toExpandedString()
        end do
        string = hangingIndent( &
                "[" // NEWLINE // join(individual_strings, "," // NEWLINE), &
                INDENTATION) // NEWLINE // "]"
    end function arrayToExpandedString

    pure function falseToString(self) result(string)
        class(JsonFalse_t), intent(in) :: self
        type(VARYING_STRING) :: string

        associate(a => self)
        end associate

        string = "false"
    end function falseToString

    pure subroutine getElementFromArray(self, position, errors, element)
        class(JsonArray_t), intent(in) :: self
        integer, intent(in) :: position
        type(ErrorList_t), intent(out) :: errors
        type(JsonElement_t), intent(out) :: element

        if (position <= 0 .or. position > self%num_elements) then
            call errors%appendError(Fatal( &
                    OUT_OF_BOUNDS_TYPE, &
                    Module_(MODULE_NAME), &
                    Procedure_("getElementFromArray"), &
                    "Attempted to access element " // toString(position) &
                        // " from an array with only " // toString(self%num_elements) &
                        // " elements"))
        else
            element = self%elements(position)
        end if
    end subroutine getElementFromArray

    pure subroutine getElementFromObjectC(self, key, errors, element)
        class(JsonObject_t), intent(in) :: self
        character(len=*), intent(in) :: key
        type(ErrorList_t), intent(out) :: errors
        type(JsonElement_t), intent(out) :: element

        type(ErrorList_t) :: errors_

        call self%getElement(var_str(key), errors_, element)
        call errors%appendErrors( &
                errors_, &
                Module_(MODULE_NAME), &
                Procedure_("getElementFromObjectC"))
    end subroutine getElementFromObjectC

    pure subroutine getElementFromObjectJ(self, key, errors, element)
        class(JsonObject_t), intent(in) :: self
        type(JsonString_t), intent(in) :: key
        type(ErrorList_t), intent(out) :: errors
        type(JsonElement_t), intent(out) :: element

        type(ErrorList_t) :: errors_

        call self%getElement(key%string, errors_, element)
        call errors%appendErrors( &
                errors_, &
                Module_(MODULE_NAME), &
                Procedure_("getElementFromObjectJ"))
    end subroutine getElementFromObjectJ

    pure subroutine getElementFromObjectS(self, key, errors, element)
        class(JsonObject_t), intent(in) :: self
        type(VARYING_STRING), intent(in) :: key
        type(ErrorList_t), intent(out) :: errors
        type(JsonElement_t), intent(out) :: element

        integer :: i

        do i = 1, self%num_members
            if (key == self%members(i)%string) then
                allocate(element%element, source = self%members(i)%element)
                return
            end if
        end do
        call errors%appendError(Fatal( &
                NOT_FOUND_TYPE, &
                Module_(MODULE_NAME), &
                Procedure_("getElementFromObjectS"), &
                '"' // key // '" not found in ' // self%toCompactString()))
    end subroutine getElementFromObjectS

    pure function getValueFromNumber(self) result(number)
        class(JsonNumber_t), intent(in) :: self
        double precision :: number

        number = self%number
    end function getValueFromNumber

    pure function getValueFromString(self) result(string)
        class(JsonString_t), intent(in) :: self
        type(VARYING_STRING) :: string

        string = self%string
    end function getValueFromString

    pure subroutine insertE(self, element, position)
        class(JsonArray_t), intent(inout) :: self
        type(JsonElement_t), intent(in) :: element
        integer, intent(in) :: position

        type(JsonElement_t) :: temp(self%num_elements)

        if (position > self%num_elements) then
            call self%append(element)
        else if (position <= 0) then
            call self%prepend(element)
        else
            temp(:) = self%elements(:)
            deallocate(self%elements)
            self%num_elements = self%num_elements + 1
            allocate(self%elements(self%num_elements))
            self%elements(1:position - 1) = temp(1:position - 1)
            self%elements(position) = element
            self%elements(position+1:) = temp(position:)
        end if
    end subroutine insertE

    pure subroutine insertV(self, value, position)
        class(JsonArray_t), intent(inout) :: self
        class(JsonValue_t), intent(in) :: value
        integer, intent(in) :: position

        call self%insert(JsonElement(value), position)
    end subroutine insertV

    pure function JsonArray(elements)
        type(JsonElement_t), intent(in) :: elements(:)
        type(JsonArray_t) :: JsonArray

        integer :: i

        JsonArray%num_elements = size(elements)
        allocate(JsonArray%elements(JsonArray%num_elements))
        do i = 1, JsonArray%num_elements
            JsonArray%elements(i) = elements(i)
        end do
    end function JsonArray

    pure function JsonElement(element)
        class(JsonValue_t), intent(in) :: element
        type(JsonElement_t) :: JsonElement

        allocate(JsonElement%element, source = element)
    end function JsonElement

    pure function JsonMemberUnsafeCE(string, element)
        character(len=*), intent(in) :: string
        type(JsonElement_t), intent(in) :: element
        type(JsonMember_t) :: JsonMemberUnsafeCE

        JsonMemberUnsafeCE = JsonMemberUnsafe(var_str(string), element%element)
    end function JsonMemberUnsafeCE

    pure function JsonMemberUnsafeCV(string, element)
        character(len=*), intent(in) :: string
        class(JsonValue_t), intent(in) :: element
        type(JsonMember_t) :: JsonMemberUnsafeCV

        JsonMemberUnsafeCV = JsonMemberUnsafe(var_str(string), element)
    end function JsonMemberUnsafeCV

    pure function JsonMemberJE(string, element)
        type(JsonString_t), intent(in) :: string
        type(JsonElement_t), intent(in) :: element
        type(JsonMember_t) :: JsonMemberJE

        JsonMemberJE = JsonMember(string, element%element)
    end function JsonMemberJE

    pure function JsonMemberJV(string, element)
        type(JsonString_t), intent(in) :: string
        class(JsonValue_t), intent(in) :: element
        type(JsonMember_t) :: JsonMemberJV

        JsonMemberJV%string = string%string
        allocate(JsonMemberJV%element, source = element)
    end function JsonMemberJV

    pure function JsonMemberUnsafeSE(string, element)
        type(VARYING_STRING), intent(in) :: string
        type(JsonElement_t), intent(in) :: element
        type(JsonMember_t) :: JsonMemberUnsafeSE

        JsonMemberUnsafeSE = JsonMemberUnsafe(string, element%element)
    end function JsonMemberUnsafeSE

    pure function JsonMemberUnsafeSV(string, element)
        type(VARYING_STRING), intent(in) :: string
        class(JsonValue_t), intent(in) :: element
        type(JsonMember_t) :: JsonMemberUnsafeSV

        JsonMemberUnsafeSV%string = string
        allocate(JsonMemberUnsafeSV%element, source = element)
    end function JsonMemberUnsafeSV

    pure function JsonNumber(number)
        double precision, intent(in) :: number
        type(JsonNumber_t) :: JsonNumber

        JsonNumber%number = number
    end function JsonNumber

    pure function JsonObjectJ(strings, elements)
        type(JsonString_t), intent(in) :: strings(:)
        type(JsonElement_t), intent(in) :: elements(size(strings))
        type(JsonObject_t) :: JsonObjectJ

        JsonObjectJ = JsonObjectUnsafe(strings%string, elements)
    end function JsonObjectJ

    pure function JsonObjectM(members)
        type(JsonMember_t), intent(in) :: members(:)
        type(JsonObject_t) :: JsonObjectM

        integer :: i

        do i = 1, size(members)
            call JsonObjectM%add(members(i))
        end do
    end function JsonObjectM

    pure function JsonObjectUnsafe(strings, elements)
        type(VARYING_STRING), intent(in) :: strings(:)
        type(JsonElement_t), intent(in) :: elements(size(strings))
        type(JsonObject_t) :: JsonObjectUnsafe

        integer :: i
        type(JsonMember_t) :: members(size(strings))

        do i = 1, size(strings)
            members(i) = JsonMemberUnsafe(strings(i), elements(i)%element)
        end do
        JsonObjectUnsafe = JsonObject(members)
    end function JsonObjectUnsafe

    pure subroutine JsonStringC(string, errors, the_string)
        character(len=*), intent(in) :: string
        type(ErrorList_t), intent(out) :: errors
        type(JsonString_t), intent(out) :: the_string

        type(ErrorList_t) :: errors_

        call JsonString(var_str(string), errors_, the_string)
        call errors%appendErrors( &
                errors_, Module_(MODULE_NAME), Procedure_("JsonStringC"))
    end subroutine JsonStringC

    pure subroutine JsonStringS(string, errors, the_string)
        type(VARYING_STRING), intent(in) :: string
        type(ErrorList_t), intent(out) :: errors
        type(JsonString_t), intent(out) :: the_string

        type(ParserOutput_t) :: parsed

        parsed = parseJsonString(newState('"' // string // '"'))
        if (parsed%ok) then
            the_string%string = string
        else
            call errors%appendError(Fatal( &
                    INVALID_INPUT_TYPE, &
                    Module_(MODULE_NAME), &
                    Procedure_("JsonStringS"), &
                    '"' // string // '" wasn''t a valid Json String: ' &
                    // parsed%message%toString()))
        end if
    end subroutine JsonStringS

    pure function JsonStringUnsafeC(string)
        character(len=*), intent(in) :: string
        type(JsonString_t) :: JsonStringUnsafeC

        JsonStringUnsafeC = JsonStringUnsafe(var_str(string))
    end function JsonStringUnsafeC

    pure function JsonStringUnsafeS(string)
        type(VARYING_STRING), intent(in) :: string
        type(JsonString_t) :: JsonStringUnsafeS

        JsonStringUnsafeS%string = string
    end function JsonStringUnsafeS

    pure function length(self)
        class(JsonArray_t), intent(in) :: self
        integer :: length

        length = self%num_elements
    end function length

    pure function nullToString(self) result(string)
        class(JsonNull_t), intent(in) :: self
        type(VARYING_STRING) :: string

        associate(a => self)
        end associate

        string = "null"
    end function nullToString

    pure function numberToString(self) result(string)
        class(JsonNumber_t), intent(in) :: self
        type(VARYING_STRING) :: string

        string = toString(self%number)
    end function numberToString

    pure function objectToCompactString(self) result(string)
        class(JsonObject_t), intent(in) :: self
        type(VARYING_STRING) :: string

        integer :: i
        type(VARYING_STRING) :: individual_strings(self%num_members)

        do i = 1, self%num_members
            individual_strings(i) = &
                    '"' // self%members(i)%string // '":' &
                    // self%members(i)%element%toCompactString()
        end do
        string = "{" // join(individual_strings, ",") // "}"
    end function objectToCompactString

    pure function objectToExpandedString(self) result(string)
        class(JsonObject_t), intent(in) :: self
        type(VARYING_STRING) :: string

        integer :: i
        type(VARYING_STRING) :: individual_strings(self%num_members)

        do i = 1, self%num_members
            individual_strings(i) = &
                    '"' // self%members(i)%string // '" : ' &
                    // self%members(i)%element%toExpandedString()
        end do
        string = hangingIndent( &
                "{" // NEWLINE // join(individual_strings, "," // NEWLINE), &
                INDENTATION) // NEWLINE // "}"
    end function objectToExpandedString

    pure function parseAnyWhitespace(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = many(parseWhitespace, state_)
    end function parseAnyWhitespace

    pure function parseArray(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        integer :: i
        type(JsonElement_t), allocatable :: the_elements(:)
        type(ParsedElement_t) :: the_value

        result_ = thenDrop( &
                dropThen(parseInitial, parseElements, state_), &
                parseCloseBracket)
        if (result_%ok) then
            select type (elements => result_%parsed)
            type is (ParsedItems_t)
                allocate(the_elements(size(elements%items)))
                do i = 1, size(the_elements)
                    select type (element => elements%items(i)%item)
                    type is (ParsedElement_t)
                        allocate(the_elements(i)%element, source = element%value_)
                    end select
                end do
            end select
            allocate(the_value%value_, source = JsonArray(the_elements))
            deallocate(result_%parsed)
            allocate(result_%parsed, source = the_value)
        end if
    contains
        pure function parseInitial(state__) result(result__)
            type(State_t), intent(in) :: state__
            type(ParserOutput_t) :: result__

            result__ = dropThen(parseOpenBracket, parseAnyWhitespace, state__)
        end function parseInitial
    end function parseArray

    pure function parseAtLeastOneDigit(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        type(VARYING_STRING), allocatable :: digits(:)
        integer :: i
        type(ParsedString_t) :: parsed_digits

        result_ = many1(parseDigit, state_)
        if (result_%ok) then
            select type (results => result_%parsed)
            type is (ParsedItems_t)
                allocate(digits(size(results%items)))
                do i = 1, size(digits)
                    select type (string => results%items(i)%item)
                    type is (ParsedCharacter_t)
                        digits(i) = string%value_
                    end select
                end do
                deallocate(result_%parsed)
                parsed_digits%value_ = join(digits, "")
                allocate(result_%parsed, source = parsed_digits)
            end select
        end if
    end function parseAtLeastOneDigit

    pure function parseCharacter(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = either(parseValidSingleCharacter, parseEscape, state_)
    end function parseCharacter

    pure function parseCharacters(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        type(VARYING_STRING), allocatable :: characters(:)
        integer :: i
        type(ParsedString_t) :: parsed_characters

        result_ = many(parseCharacter, state_)
        if (result_%ok) then
            select type (results => result_%parsed)
            type is (ParsedItems_t)
                allocate(characters(size(results%items)))
                do i = 1, size(characters)
                    select type (the_character => results%items(i)%item)
                    type is (ParsedString_t)
                        characters(i) = the_character%value_
                    end select
                end do
                deallocate(result_%parsed)
                parsed_characters%value_ = join(characters, "")
                allocate(result_%parsed, source = parsed_characters)
            end select
        end if
    end function parseCharacters

    pure function parseCloseBrace(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = parseChar("}", state_)
    end function parseCloseBrace

    pure function parseCloseBracket(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = parseChar("]", state_)
    end function parseCloseBracket

    pure function parseColon(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = parseChar(":", state_)
    end function parseColon

    pure function parseComma(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = parseChar(",", state_)
    end function parseComma

    pure function parseDigits(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        type(VARYING_STRING), allocatable :: digits(:)
        integer :: i
        type(ParsedString_t) :: parsed_digits

        result_ = many(parseDigit, state_)
        select type (results => result_%parsed)
        type is (ParsedItems_t)
            allocate(digits(size(results%items)))
            do i = 1, size(digits)
                select type (string => results%items(i)%item)
                type is (ParsedCharacter_t)
                    digits(i) = string%value_
                end select
            end do
        end select
        deallocate(result_%parsed)
        parsed_digits%value_ = join(digits, "")
        allocate(result_%parsed, source = parsed_digits)
    end function parseDigits

    pure function parseElement(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = thenDrop( &
                dropThen(parseAnyWhitespace, parseValue, state_), &
                parseAnyWhitespace)
    end function parseElement

    pure function parseElements(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = manyWithSeparator(parseElement, parseComma, state_)
    end function parseElements

    pure function parseEscape(the_state) result(the_result)
        type(State_t), intent(in) :: the_state
        type(ParserOutput_t) :: the_result

        the_result = dropThen(parseBackslash, parseEscapedCharacter, the_state)
        if (the_result%ok) then
            select type (the_string => the_result%parsed)
            type is (ParsedString_t)
                the_string%value_ = "\" // the_string%value_
            end select
        end if
    contains
        pure function parseBackslash(state_) result(result_)
            type(State_t), intent(in) :: state_
            type(ParserOutput_t) :: result_

            result_ = parseChar("\", state_)
        end function parseBackslash
    end function parseEscape

    pure function parseEscapedCharacter(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = either( &
                parseSingleEscapedCharacter, parseEscapedUnicode, state_)
    end function parseEscapedCharacter

    pure function parseEscapedUnicode(the_state) result(the_result)
        type(State_t), intent(in) :: the_state
        type(ParserOutput_t) :: the_result

        the_result = dropThen(parseU, parseHexDigits, the_state)
        if (the_result%ok) then
            select type (the_string => the_result%parsed)
            type is (ParsedString_t)
                the_string%value_ = "u" // the_string%value_
            end select
        end if
    contains
        pure function parseU(state_) result(result_)
            type(State_t), intent(in) :: state_
            type(ParserOutput_t) :: result_

            result_ = parseChar("u", state_)
        end function parseU
    end function parseEscapedUnicode

    pure function parseExponent(the_state) result(the_result)
        type(State_t), intent(in) :: the_state
        type(ParserOutput_t) :: the_result

        the_result = sequence( &
                sequence(parseE, thenParseSign, the_state), &
                thenParseAtLeastOneDigit)
    contains
        pure function parseE(state_) result(result_)
            type(State_t), intent(in) :: state_
            type(ParserOutput_t) :: result_

            type(ParsedString_t) :: the_string

            result_ = either(parseUpper, parseLower, state_)
            if (result_%ok) then
                select type (the_character => result_%parsed)
                type is (ParsedCharacter_t)
                    the_string%value_ = the_character%value_
                    deallocate(result_%parsed)
                    allocate(result_%parsed, source = the_string)
                end select
            end if
        end function parseE

        pure function parseUpper(state_) result(result_)
            type(State_t), intent(in) :: state_
            type(ParserOutput_t) :: result_

            result_ = parseChar("E", state_)
        end function parseUpper

        pure function parseLower(state_) result(result_)
            type(State_t), intent(in) :: state_
            type(ParserOutput_t) :: result_

            result_ = parseChar("e", state_)
        end function parseLower
    end function parseExponent

    pure function parseFalse(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        type(ParsedElement_t) :: the_value

        result_ = parseString("false", state_)
        if (result_%ok) then
            deallocate(result_%parsed)
            allocate(the_value%value_, source = JSON_FALSE)
            allocate(result_%parsed, source = the_value)
        end if
    end function parseFalse

    pure function parseFraction(the_state) result(the_result)
        type(State_t), intent(in) :: the_state
        type(ParserOutput_t) :: the_result

        the_result = sequence( &
                parseDecimalPoint, thenParseAtLeastOneDigit, the_state)
    contains
        pure function parseDecimalPoint(state_) result(result_)
            type(State_t), intent(in) :: state_
            type(ParserOutput_t) :: result_

            type(ParsedString_t) :: the_string

            result_ = parseChar(".", state_)
            if (result_%ok) then
                select type (the_character => result_%parsed)
                type is (ParsedCharacter_t)
                    the_string%value_ = the_character%value_
                    deallocate(result_%parsed)
                    allocate(result_%parsed, source = the_string)
                end select
            end if
        end function parseDecimalPoint
    end function parseFraction

    pure function parseHexDigit(the_state) result(the_result)
        type(State_t), intent(in) :: the_state
        type(ParserOutput_t) :: the_result

        the_result = withLabel("hex digit", theParser, the_state)
    contains
        pure function theParser(state_) result(result_)
            type(State_t), intent(in) :: state_
            type(ParserOutput_t) :: result_

            type(ParsedString_t) :: the_string

            result_ = satisfy(theMatcher, state_)
            if (result_%ok) then
                select type (the_character => result_%parsed)
                type is (ParsedCharacter_t)
                    the_string%value_ = the_character%value_
                    deallocate(result_%parsed)
                    allocate(result_%parsed, source = the_string)
                end select
            end if
        end function theParser

        pure function theMatcher(char_) result(matches)
            character(len=1), intent(in) :: char_
            logical :: matches

            matches = "0123456789abcdefABCDEF".includes.char_
        end function theMatcher
    end function parseHexDigit

    pure function parseHexDigits(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        type(VARYING_STRING) :: digits(4)
        integer :: i
        type(ParsedString_t) :: parsed_digits

        result_ = repeat_(parseHexDigit, 4, state_)
        if (result_%ok) then
            select type (results => result_%parsed)
            type is (ParsedItems_t)
                do i = 1, 4
                    select type (string => results%items(i)%item)
                    type is (ParsedString_t)
                        digits(i) = string%value_
                    end select
                end do
            end select
            deallocate(result_%parsed)
            parsed_digits%value_ = join(digits, "")
            allocate(result_%parsed, source = parsed_digits)
        end if
    end function parseHexDigits

    pure function parseInteger(the_state) result(the_result)
        type(State_t), intent(in) :: the_state
        type(ParserOutput_t) :: the_result

        the_result = either(firstTwo, secondTwo, the_state)
    contains
        pure function firstTwo(state_) result(result_)
            type(State_t), intent(in) :: state_
            type(ParserOutput_t) :: result_

            result_ = either(parseSingleDigit, parseMultipleDigits, state_)
        end function firstTwo

        pure function secondTwo(state_) result(result_)
            type(State_t), intent(in) :: state_
            type(ParserOutput_t) :: result_

            result_ = either( &
                    parseNegativeSingleDigit, &
                    parseNegativeMultipleDigits, &
                    state_)
        end function secondTwo
    end function parseInteger

    pure subroutine parseJsonC(string, errors, json)
        character(len=*), intent(in) :: string
        type(ErrorList_t), intent(out) :: errors
        type(JsonElement_t), intent(out) :: json

        type(ErrorList_t) :: errors_

        call parseJson(var_str(string), errors_, json)
        call errors%appendErrors( &
                errors_, Module_(MODULE_NAME), Procedure_("parseJsonC"))
    end subroutine parseJsonC

    pure subroutine parseJsonS(string, errors, json)
        type(VARYING_STRING), intent(in) :: string
        type(ErrorList_t), intent(out) :: errors
        type(JsonElement_t), intent(out) :: json

        type(ParseResult_t) :: result_

        result_ = parseWith(parseElement, string)
        if (result_%ok) then
            select type (the_json => result_%parsed)
            type is (ParsedElement_t)
                json = JsonElement(the_json%value_)
            end select
        else
            call errors%appendError(Fatal( &
                    INVALID_INPUT_TYPE, &
                    Module_(MODULE_NAME), &
                    Procedure_("parseJsonS"), &
                    result_%message))
        end if
    end subroutine parseJsonS

    subroutine parseJsonFromFileC(filename, errors, json)
        character(len=*), intent(in) :: filename
        type(ErrorList_t), intent(out) :: errors
        type(JsonElement_t), intent(out) :: json

        type(ErrorList_t) :: errors_

        call parseJson(readFile(filename), errors_, json)
        call errors%appendErrors( &
                errors_, Module_(MODULE_NAME), Procedure_("parseJsonFromFileC"))
    end subroutine parseJsonFromFileC

    subroutine parseJsonFromFileS(filename, errors, json)
        type(VARYING_STRING), intent(in) :: filename
        type(ErrorList_t), intent(out) :: errors
        type(JsonElement_t), intent(out) :: json

        type(ErrorList_t) :: errors_

        call parseJsonFromFile(char(filename), errors_, json)
        call errors%appendErrors( &
                errors_, Module_(MODULE_NAME), Procedure_("parseJsonFromFileS"))
    end subroutine parseJsonFromFileS

    pure function parseJsonString(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        type(ParsedElement_t) :: the_value

        result_ = thenDrop( &
                dropThen(parseQuote, parseCharacters, state_), &
                parseQuote)
        if (result_%ok) then
            select type (parsed_string => result_%parsed)
            type is (ParsedString_t)
                allocate(the_value%value_, source = &
                        JsonStringUnsafe(parsed_string%value_))
                deallocate(result_%parsed)
                allocate(result_%parsed, source = the_value)
            end select
        end if
    end function parseJsonString

    pure function parseMember(the_state) result(the_result)
        type(State_t), intent(in) :: the_state
        type(ParserOutput_t) :: the_result

        the_result = sequence( &
                thenDrop( &
                        thenDrop( &
                                dropThen( &
                                        parseAnyWhitespace, &
                                        parseJsonString, &
                                        the_state), &
                                parseAnyWhitespace), &
                        parseColon), &
                thenParseElement)
    contains
        pure function thenParseElement(previous, state_) result(result_)
            class(ParsedValue_t), intent(in) :: previous
            type(State_t), intent(in) :: state_
            type(ParserOutput_t) :: result_

            type(ParsedMember_t) :: member

            select type (previous)
            type is (ParsedElement_t)
                select type (string => previous%value_)
                type is (JsonString_t)
                    result_ = parseElement(state_)
                    if (result_%ok) then
                        select type (element => result_%parsed)
                        type is (ParsedElement_t)
                            member%string = string%string
                            allocate(member%element, source = element%value_)
                            deallocate(result_%parsed)
                            allocate(result_%parsed, source = member)
                        end select
                    end if
                end select
            end select
        end function thenParseElement
    end function parseMember

    pure function parseMembers(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = manyWithSeparator(parseMember, parseComma, state_)
    end function parseMembers

    pure function parseMinus(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        type(ParsedString_t) :: the_string

        result_ = parseChar("-", state_)
        if (result_%ok) then
            select type (the_character => result_%parsed)
            type is (ParsedCharacter_t)
                the_string%value_ = the_character%value_
                deallocate(result_%parsed)
                allocate(result_%parsed, source = the_string)
            end select
        end if
    end function parseMinus

    pure function parseMultipleDigits(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = sequence(parseOneNine, thenParseDigits, state_)
    end function parseMultipleDigits

    pure function parseNegativeMultipleDigits(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = sequence( &
                sequence(parseMinus, thenParseOneNine, state_), &
                thenParseDigits)
    end function parseNegativeMultipleDigits

    pure function parseNegativeSingleDigit(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = sequence(parseMinus, thenParseDigit, state_)
    end function parseNegativeSingleDigit

    pure function parseNull(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        type(ParsedElement_t) :: the_value

        result_ = parseString("null", state_)
        if (result_%ok) then
            deallocate(result_%parsed)
            allocate(the_value%value_, source = JSON_NULL)
            allocate(result_%parsed, source = the_value)
        end if
    end function parseNull

    pure function parseNumber(the_state) result(the_result)
        type(State_t), intent(in) :: the_state
        type(ParserOutput_t) :: the_result

        the_result = withLabel("number", theParser, the_state)
    contains
        pure function theParser(state_) result(result_)
            type(State_t), intent(in) :: state_
            type(ParserOutput_t) :: result_

            double precision :: the_number
            character(len=64) :: the_string
            type(ParsedElement_t) :: the_value

            result_ = sequence( &
                    sequence(parseInteger, thenParseFraction, state_), &
                    thenParseExponent)
            if (result_%ok) then
                select type (parsed_string => result_%parsed)
                type is (ParsedString_t)
                    the_string = parsed_string%value_
                    read(the_string, *) the_number
                    allocate(the_value%value_, source = JsonNumber(the_number))
                    deallocate(result_%parsed)
                    allocate(result_%parsed, source = the_value)
                end select
            end if
        end function theParser
    end function parseNumber

    pure function parseObject(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        integer :: i
        type(JsonMember_t), allocatable :: the_members(:)
        type(ParsedElement_t) :: the_value

        result_ = thenDrop( &
                dropThen(parseInitial, parseMembers, state_), &
                parseCloseBrace)
        if (result_%ok) then
            select type (members => result_%parsed)
            type is (ParsedItems_t)
                allocate(the_members(size(members%items)))
                do i = 1, size(the_members)
                    select type (member => members%items(i)%item)
                    type is (ParsedMember_t)
                        the_members(i) = JsonMemberUnsafe(member%string, member%element)
                    end select
                end do
                allocate(the_value%value_, source = JsonObject(the_members))
                deallocate(result_%parsed)
                allocate(result_%parsed, source = the_value)
            end select
        end if
    contains
        pure function parseInitial(state__) result(result__)
            type(State_t), intent(in) :: state__
            type(ParserOutput_t) :: result__

            result__ = dropThen(parseOpenBrace, parseAnyWhitespace, state__)
        end function parseInitial
    end function parseObject

    pure function parseOneNine(the_state) result(the_result)
        type(State_t), intent(in) :: the_state
        type(ParserOutput_t) :: the_result

        the_result = withLabel("1-9", theParser, the_state)
    contains
        pure function theParser(state_) result(result_)
            type(State_t), intent(in) :: state_
            type(ParserOutput_t) :: result_

            type(ParsedString_t) :: the_string

            result_ = satisfy(theMatcher, state_)
            if (result_%ok) then
                select type (the_character => result_%parsed)
                type is (ParsedCharacter_t)
                    the_string%value_ = the_character%value_
                    deallocate(result_%parsed)
                    allocate(result_%parsed, source = the_string)
                end select
            end if
        end function theParser

        pure function theMatcher(char_) result(matches)
            character(len=1), intent(in) :: char_
            logical :: matches

            matches = "123456789".includes.char_
        end function theMatcher
    end function parseOneNine

    pure function parseOpenBrace(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = parseChar("{", state_)
    end function parseOpenBrace

    pure function parseOpenBracket(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = parseChar("[", state_)
    end function parseOpenBracket

    pure function parseQuote(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = parseChar('"', state_)
    end function parseQuote

    pure function parseSign(the_state) result(the_result)
        type(State_t), intent(in) :: the_state
        type(ParserOutput_t) :: the_result

        the_result = either(parsePlus, parseMinus, the_state)
    contains
        pure function parsePlus(state_) result(result_)
            type(State_t), intent(in) :: state_
            type(ParserOutput_t) :: result_

            type(ParsedString_t) :: the_string

            result_ = parseChar("+", state_)
            if (result_%ok) then
                select type (the_character => result_%parsed)
                type is (ParsedCharacter_t)
                    the_string%value_ = the_character%value_
                    deallocate(result_%parsed)
                    allocate(result_%parsed, source = the_string)
                end select
            end if
        end function parsePlus
    end function parseSign

    pure function parseSingleDigit(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        type(ParsedString_t) :: the_string

        result_ = parseDigit(state_)
        if (result_%ok) then
            select type (the_character => result_%parsed)
            type is (ParsedCharacter_t)
                the_string%value_ = the_character%value_
                deallocate(result_%parsed)
                allocate(result_%parsed, source = the_string)
            end select
        end if
    end function parseSingleDigit

    pure function parseSingleEscapedCharacter(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        type(ParsedString_t) :: the_string

        result_ = satisfy(theMatcher, state_)
        if (result_%ok) then
            select type (the_character => result_%parsed)
            type is (ParsedCharacter_t)
                the_string%value_ = the_character%value_
                deallocate(result_%parsed)
                allocate(result_%parsed, source = the_string)
            end select
        end if
    contains
        pure function theMatcher(char_) result(matches)
            character(len=1), intent(in) :: char_
            logical :: matches

            matches = &
                    char_ == '"' &
                    .or. char_ == '\' &
                    .or. char_ == '/' &
                    .or. char_ == 'b' &
                    .or. char_ == 'f' &
                    .or. char_ == 'n' &
                    .or. char_ == 'r' &
                    .or. char_ == 't'
        end function theMatcher
    end function parseSingleEscapedCharacter

    pure function parseTrue(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        type(ParsedElement_t) :: the_value

        result_ = parseString("true", state_)
        if (result_%ok) then
            deallocate(result_%parsed)
            allocate(the_value%value_, source = JSON_TRUE)
            allocate(result_%parsed, source = the_value)
        end if
    end function parseTrue

    pure function parseValidSingleCharacter(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        type(ParsedString_t) :: the_string

        result_ = satisfy(theMatcher, state_)
        if (result_%ok) then
            select type (the_character => result_%parsed)
            type is (ParsedCharacter_t)
                the_string%value_ = the_character%value_
                deallocate(result_%parsed)
                allocate(result_%parsed, source = the_string)
            end select
        end if
    contains
        pure function theMatcher(char_) result(matches)
            character(len=1), intent(in) :: char_
            logical :: matches

            matches = char_ /= '"' .and. char_ /= '\'
        end function theMatcher
    end function parseValidSingleCharacter

    pure function parseValue(state_) result(result_)
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        result_ = either(firstTry, parseObject, state_)
    contains
        pure function firstTry(state__) result(result__)
            type(State_t), intent(in) :: state__
            type(ParserOutput_t) :: result__

            result__ = either(parseNull, secondTry, state__)
        end function firstTry

        pure function secondTry(state__) result(result__)
            type(State_t), intent(in) :: state__
            type(ParserOutput_t) :: result__

            result__ = either(parseTrue, thridTry, state__)
        end function secondTry

        pure function thridTry(state__) result(result__)
            type(State_t), intent(in) :: state__
            type(ParserOutput_t) :: result__

            result__ = either(parseFalse, fourthTry, state__)
        end function thridTry

        pure function fourthTry(state__) result(result__)
            type(State_t), intent(in) :: state__
            type(ParserOutput_t) :: result__

            result__ = either(parseNumber, fifthTry, state__)
        end function fourthTry

        pure function fifthTry(state__) result(result__)
            type(State_t), intent(in) :: state__
            type(ParserOutput_t) :: result__

            result__ = either(parseJsonString, parseArray, state__)
        end function fifthTry
    end function parseValue

    pure subroutine prependE(self, element)
        class(JsonArray_t), intent(inout) :: self
        type(JsonElement_t), intent(in) :: element

        type(JsonElement_t) :: temp(self%num_elements)

        if (self%num_elements > 0) then
            temp(1:self%num_elements) = self%elements(1:self%num_elements)
            deallocate(self%elements)
            self%num_elements = self%num_elements + 1
            allocate(self%elements(self%num_elements))
            self%elements(1) = element
            self%elements(2:self%num_elements) = temp(1:)
        else
            self%num_elements = 1
            allocate(self%elements(1))
            self%elements(1) = element
        end if
    end subroutine prependE

    pure subroutine prependV(self, value)
        class(JsonArray_t), intent(inout) :: self
        class(JsonValue_t), intent(in) :: value

        call self%prepend(JsonElement(value))
    end subroutine prependV

    pure function stringToString(self) result(string)
        class(JsonString_t), intent(in) :: self
        type(VARYING_STRING) :: string

        string = '"' // self%string // '"'
    end function stringToString

    pure function thenParseAtLeastOneDigit(previous, state_) result(result_)
        class(ParsedValue_t), intent(in) :: previous
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        select type (previous)
        type is (ParsedString_t)
            result_ = parseAtLeastOneDigit(state_)
            if (result_%ok) then
                select type (next => result_%parsed)
                type is (ParsedString_t)
                    next%value_ = previous%value_ // next%value_
                end select
            end if
        end select
    end function thenParseAtLeastOneDigit

    pure function thenParseDigit(previous, state_) result(result_)
        class(ParsedValue_t), intent(in) :: previous
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        type(ParsedString_t) :: the_string

        select type (previous)
        type is (ParsedString_t)
            result_ = parseDigit(state_)
            if (result_%ok) then
                select type (next => result_%parsed)
                type is (ParsedCharacter_t)
                    the_string%value_ = previous%value_ // next%value_
                    deallocate(result_%parsed)
                    allocate(result_%parsed, source = the_string)
                end select
            end if
        end select
    end function thenParseDigit

    pure function thenParseDigits(previous, state_) result(result_)
        class(ParsedValue_t), intent(in) :: previous
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        select type (previous)
        type is (ParsedString_t)
            result_ = parseDigits(state_)
            if (result_%ok) then
                select type (next => result_%parsed)
                type is (ParsedString_t)
                    next%value_ = previous%value_ // next%value_
                end select
            end if
        end select
    end function thenParseDigits

    pure function thenParseExponent(previous, state_) result(result_)
        class(ParsedValue_t), intent(in) :: previous
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        select type (previous)
        type is (ParsedString_t)
            result_ = parseExponent(state_)
            if (result_%ok) then
                select type (next => result_%parsed)
                type is (ParsedString_t)
                    next%value_ = previous%value_ // next%value_
                end select
            else
                result_ = EmptyOk( &
                        previous, &
                        state_%input, &
                        state_%position, &
                        Message(state_%position, var_str(""), [VARYING_STRING::]))
            end if
        end select
    end function thenParseExponent

    pure function thenParseFraction(previous, state_) result(result_)
        class(ParsedValue_t), intent(in) :: previous
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        select type (previous)
        type is (ParsedString_t)
            result_ = parseFraction(state_)
            if (result_%ok) then
                select type (next => result_%parsed)
                type is (ParsedString_t)
                    next%value_ = previous%value_ // next%value_
                end select
            else
                result_ = EmptyOk( &
                        previous, &
                        state_%input, &
                        state_%position, &
                        Message(state_%position, var_str(""), [VARYING_STRING::]))
            end if
        end select
    end function thenParseFraction

    pure function thenParseOneNine(previous, state_) result(result_)
        class(ParsedValue_t), intent(in) :: previous
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        select type (previous)
        type is (ParsedString_t)
            result_ = parseOneNine(state_)
            if (result_%ok) then
                select type (next => result_%parsed)
                type is (ParsedString_t)
                    next%value_ = previous%value_ // next%value_
                end select
            end if
        end select
    end function thenParseOneNine

    pure function thenParseSign(previous, state_) result(result_)
        class(ParsedValue_t), intent(in) :: previous
        type(State_t), intent(in) :: state_
        type(ParserOutput_t) :: result_

        select type (previous)
        type is (ParsedString_t)
            result_ = parseSign(state_)
            if (result_%ok) then
                select type (next => result_%parsed)
                type is (ParsedString_t)
                    next%value_ = previous%value_ // next%value_
                end select
            else
                result_ = EmptyOk( &
                        previous, &
                        state_%input, &
                        state_%position, &
                        Message(state_%position, var_str(""), [VARYING_STRING::]))
            end if
        end select
    end function thenParseSign

    pure function trueToString(self) result(string)
        class(JsonTrue_t), intent(in) :: self
        type(VARYING_STRING) :: string

        associate(a => self)
        end associate

        string = "true"
    end function trueToString
end module jsonff
