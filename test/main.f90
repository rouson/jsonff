program test
    implicit none

    call run()
contains
    subroutine run()
        use json_test, only: &
            json_json => test_json
        use parse_json_test, only: &
            parse_json_parse_json => test_parse_json
        use iso_varying_string
        use Vegetables_m, only: TestItem_t, testThat, runTests

        type(TestItem_t) :: tests
        type(TestItem_t) :: individual_tests(2)

        individual_tests(1) = json_json()
        individual_tests(2) = parse_json_parse_json()
        tests = testThat(individual_tests)

        call runTests(tests)
    end subroutine run
end program test
