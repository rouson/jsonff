module json_test
    use erloff, only: ErrorList_t, NOT_FOUND_TYPE, OUT_OF_BOUNDS_TYPE
    use iso_varying_string, only: VARYING_STRING
    use jsonff, only: &
            JsonArray_t, &
            JsonElement_t, &
            JsonNumber_t, &
            JsonObject_t, &
            JsonString_t, &
            JsonNumber, &
            JsonString, &
            JsonStringUnsafe, &
            INVALID_INPUT_TYPE, &
            JSON_FALSE, &
            JSON_NULL, &
            JSON_TRUE
    use strff, only: NEWLINE
    use Vegetables_m, only: &
            Result_t, &
            TestItem_t, &
            assertEquals, &
            assertIncludes, &
            assertNot, &
            assertThat, &
            Describe, &
            It

    implicit none
    private

    public :: test_json
contains
    function test_json() result(tests)
        type(TestItem_t) :: tests

        type(TestItem_t) :: individual_tests(14)

        individual_tests(1) = It( &
                "null has the correct string representation", &
                checkNullToString)
        individual_tests(2) = It( &
                "true has the correct string representation", &
                checkTrueToString)
        individual_tests(3) = It( &
                "false has the correct string representation", &
                checkFalseToString)
        individual_tests(4) = It( &
                "a string can be converted back", checkStringToString)
        individual_tests(5) = It( &
                "a number has the correct string representation", &
                checkNumberToString)
        individual_tests(6) = It( &
                "an array has the correct string representation", &
                checkArrayToString)
        individual_tests(7) = It( &
                "an object has the correct string representation", &
                checkObjectToString)
        individual_tests(8) = It( &
                "a complex object has the correct string representation", &
                checkComplexObjectToString)
        individual_tests(9) = It( &
                "can be generated in an expaned, easier to read form", &
                checkPrettyPrinting)
        individual_tests(10) = It( &
                "can extract a value from an array", getValueFromArray)
        individual_tests(11) = It( &
                "extracting a value from an array with a position greater than the number of elements in the array is an error", &
                getValueFromArrayFailure)
        individual_tests(12) = It( &
                "can extract a value from an object", getValueFromObject)
        individual_tests(13) = It( &
                "extracting a value from an object with a key it doesn't have is an error", &
                getValueFromObjectFailure)
        individual_tests(14) = It( &
                "trying to create an invalid string is an error", &
                checkStringError)
        tests = Describe("JSON", individual_tests)
    end function test_json

    pure function checkNullToString() result(result_)
        type(Result_t) :: result_

        result_ = assertEquals("null", JSON_NULL%toCompactString())
    end function checkNullToString

    pure function checkTrueToString() result(result_)
        type(Result_t) :: result_

        result_ = assertEquals("true", JSON_TRUE%toCompactString())
    end function checkTrueToString

    pure function checkFalseToString() result(result_)
        type(Result_t) :: result_

        result_ = assertEquals("false", JSON_FALSE%toCompactString())
    end function checkFalseToString

    pure function checkStringToString() result(result_)
        type(Result_t) :: result_

        type(JsonString_t) :: string

        string = JsonStringUnsafe("Hello")

        result_ = assertEquals('"Hello"', string%toCompactString())
    end function checkStringToString

    pure function checkNumberToString() result(result_)
        type(Result_t) :: result_

        type(JsonNumber_t) :: number

        number = JsonNumber(1.0d0)

        result_ = assertEquals("1.0", number%toCompactString())
    end function checkNumberToString

    pure function checkArrayToString() result(result_)
        type(Result_t) :: result_

        type(JsonArray_t) :: array

        call array%append(JSON_NULL)
        call array%append(JsonStringUnsafe("Hello"))
        call array%append(JsonNumber(2.0d0))
        result_ = assertEquals('[null,"Hello",2.0]', array%toCompactString())
    end function checkArrayToString

    pure function checkObjectToString() result(result_)
        type(Result_t) :: result_

        type(JsonObject_t) :: object
        type(VARYING_STRING) :: string

        call object%addUnsafe("sayHello", JSON_TRUE)
        call object%addUnsafe("aNumber", JsonNumber(3.0d0))
        string = object%toCompactString()

        result_ = &
                assertIncludes('"sayHello":true', string) &
                .and.assertIncludes('"aNumber":3.0', string) &
                .and.assertIncludes("{", string) &
                .and.assertIncludes("}", string) &
                .and.assertIncludes(",", string)
    end function checkObjectToString

    pure function checkComplexObjectToString() result(result_)
        type(Result_t) :: result_

        character(len=*), parameter :: EXPECTED = &
                '{"Hello":[null,{"World":1.0},true]}'
        type(JsonArray_t) :: array
        type(JsonObject_t) :: inner_object
        type(JsonObject_t) :: outer_object

        call inner_object%addUnsafe("World", JsonNumber(1.0d0))
        call array%append(JSON_NULL)
        call array%append(inner_object)
        call array%append(JSON_TRUE)
        call outer_object%addUnsafe("Hello", array)

        result_ = assertEquals(EXPECTED, outer_object%toCompactString())
    end function checkComplexObjectToString

    pure function checkPrettyPrinting() result(result_)
        type(Result_t) :: result_

        character(len=*), parameter :: EXPECTED = &
   '{' // NEWLINE &
// '    "Hello" : [' // NEWLINE &
// '        null,' // NEWLINE &
// '        {' // NEWLINE &
// '            "World" : 1.0' // NEWLINE &
// '        },' // NEWLINE &
// '        true' // NEWLINE &
// '    ]' // NEWLINE &
// '}'
        type(JsonArray_t) :: array
        type(JsonObject_t) :: inner_object
        type(JsonObject_t) :: outer_object

        call inner_object%addUnsafe("World", JsonNumber(1.0d0))
        call array%append(JSON_NULL)
        call array%append(inner_object)
        call array%append(JSON_TRUE)
        call outer_object%addUnsafe("Hello", array)

        result_ = assertEquals(EXPECTED, outer_object%toExpandedString())
    end function checkPrettyPrinting

    pure function getValueFromArray() result(result_)
        type(Result_t) :: result_

        type(JsonArray_t) :: array
        type(ErrorList_t) :: errors
        type(JsonElement_t) :: retrieved

        call array%append(JsonStringUnsafe("first"))
        call array%append(JsonStringUnsafe("second"))
        call array%append(JsonStringUnsafe("third"))

        call array%getElement(3, errors, retrieved)

        result_ = assertNot(errors%hasAny(), errors%toString())
        if (result_%passed()) then
            result_ = assertEquals( &
                    '"third"', retrieved%element%toCompactString())
        end if
    end function getValueFromArray

    pure function getValueFromArrayFailure() result(result_)
        type(Result_t) :: result_

        type(JsonArray_t) :: array
        type(ErrorList_t) :: errors
        type(JsonElement_t) :: retrieved

        call array%append(JsonStringUnsafe("first"))
        call array%append(JsonStringUnsafe("second"))
        call array%append(JsonStringUnsafe("third"))

        call array%getElement(4, errors, retrieved)

        result_ = assertThat( &
                errors.hasType.OUT_OF_BOUNDS_TYPE, errors%toString())
    end function getValueFromArrayFailure

    pure function getValueFromObject() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(JsonObject_t) :: object
        type(JsonElement_t) :: retrieved

        call object%addUnsafe("first", JsonStringUnsafe("hello"))
        call object%addUnsafe("second", JsonStringUnsafe("goodbye"))

        call object%getElement("first", errors, retrieved)

        result_ = assertNot(errors%hasAny(), errors%toString())
        if (result_%passed()) then
            result_ = assertEquals( &
                    '"hello"', retrieved%element%toCompactString())
        end if
    end function getValueFromObject

    pure function getValueFromObjectFailure() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(JsonObject_t) :: object
        type(JsonElement_t) :: retrieved

        call object%addUnsafe("first", JsonStringUnsafe("hello"))
        call object%addUnsafe("second", JsonStringUnsafe("goodbye"))

        call object%getElement("third", errors, retrieved)

        result_ = assertThat(errors.hasType.NOT_FOUND_TYPE, errors%toString())
    end function getValueFromObjectFailure

    pure function checkStringError() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(JsonString_t) :: string

        call JsonString("\invalid", errors, string)

        result_ = assertThat(errors.hasType.INVALID_INPUT_TYPE, errors%toString())
    end function checkStringError
end module json_test
